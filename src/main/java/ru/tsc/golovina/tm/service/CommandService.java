package ru.tsc.golovina.tm.service;

import ru.tsc.golovina.tm.api.repository.ICommandRepository;
import ru.tsc.golovina.tm.api.service.ICommandService;
import ru.tsc.golovina.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
