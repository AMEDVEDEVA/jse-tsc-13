package ru.tsc.golovina.tm.controller;

import ru.tsc.golovina.tm.api.controller.IProjectTaskController;
import ru.tsc.golovina.tm.api.controller.ITaskController;
import ru.tsc.golovina.tm.api.service.IProjectService;
import ru.tsc.golovina.tm.api.service.IProjectTaskService;
import ru.tsc.golovina.tm.api.service.ITaskService;
import ru.tsc.golovina.tm.model.Project;
import ru.tsc.golovina.tm.model.Task;
import ru.tsc.golovina.tm.util.TerminalUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectService projectService;
    private final ITaskService taskService;
    private final IProjectTaskService projectTaskService;
    private final ITaskController taskController;

    public ProjectTaskController(
            final IProjectService projectService,
            final ITaskService taskService,
            final IProjectTaskService projectTaskService,
            final ITaskController taskController) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
        this.taskController = taskController;
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task taskUpdated = projectTaskService.bindTaskById(projectId, taskId);
        if (taskUpdated == null) System.out.println("Incorrect values");
        System.out.println("[OK]");
    }

    @Override
    public void unbindTaskByProjectId() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("[ENTER TASK ID]");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findById(taskId);
        if (task == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task taskUpdated = projectTaskService.unbindTaskById(projectId, taskId);
        if (taskUpdated == null) System.out.println("Incorrect values");
        System.out.println("[OK]");
    }

    @Override
    public void showTaskByProjectId() {
        System.out.println("[ENTER PROJECT ID]");
        final String projectId = TerminalUtil.nextLine();
        final Project project = projectService.findById(projectId);
        if (project == null) {
            System.out.println("Incorrect values");
            return;
        }
        final List<Task> tasks = projectTaskService.findTaskByProjectId(projectId);
        if (tasks.size() <= 0) System.out.println("List is empty");
        for (Task task : tasks)
            taskController.showTask(task);
    }

    @Override
    public void removeProjectById() {
        System.out.println("[ENTER PROJECT ID]");
        final String id = TerminalUtil.nextLine();
        if (id == null || id.isEmpty()) {
            System.out.println("Incorrect values");
            return;
        }
        projectTaskService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[ENTER PROJECT INDEX]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (index == null || index < 0) {
            System.out.println("Incorrect values");
            return;
        }
        projectTaskService.removeByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByName() {
        System.out.println("[ENTER PROJECT NAME]");
        final String name = TerminalUtil.nextLine();
        if (name == null || name.isEmpty()) {
            System.out.println("Incorrect values");
            return;
        }
        projectTaskService.removeByName(name);
        System.out.println("[OK]");
    }

}
